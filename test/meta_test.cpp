/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file meta_test.cpp
 * @author Niels Kristian Kjærgård Madsen
 * @date Fri Jun 24 2022
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/

#include <catch2/catch.hpp>

#include <tulib/meta/type_traits.hpp>

#include <test/meta_test.hpp>

#include <map>
#include <vector>

TEST_CASE("Template test", "[meta]")
{
    tulib::test::template_argument_test::run();
}

TEST_CASE("Function-traits test", "[meta]")
{
    tulib::test::function_traits_test::run();
}

TEST_CASE("Type traits", "[meta]")
{
    static_assert(tulib::meta::is_tuple_v<std::tuple<>>, "Empty tuple is not detected.");
    static_assert(tulib::meta::is_tuple_v<std::pair<int, double>>, "pair(int, double) not detected.");
    static_assert(!tulib::meta::is_tuple_v<std::array<int, 6>>, "std::array: false positive");
    static_assert(!tulib::meta::is_tuple_v<std::vector<float>>, "std::vector: false positive");

    static_assert(tulib::meta::is_instance_of_v<std::vector, std::vector<int>>, "std::vector<int> is instance of std::vector.");
    static_assert(tulib::meta::is_instance_of_v<std::vector, std::vector<double>>, "std::vector<double> is instance of std::vector.");
    static_assert(tulib::meta::is_instance_of_v<std::map, std::map<int, double>>, "map<int, double> is instance of std::map.");
    static_assert(!tulib::meta::is_instance_of_v<std::map, std::vector<char>>, "vector<char> is not a map");
}
