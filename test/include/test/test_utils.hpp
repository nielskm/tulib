/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file test_utils.hpp
 * @author Niels Kristian Kjærgård Madsen
 * @date Fri Jun 24 2022
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/

#pragma once

#include <catch2/catch.hpp>
#include <random>
#include <optional>

#include <tulib/meta/complex.hpp>

namespace tulib::test
{
/**
 * Singleton random-number generator seeded using a std::random_device.
 **/
auto& get_rng()
{
    static std::mt19937 rng
        (   []() -> std::seed_seq&
            {
                static std::random_device rd;
                static std::seed_seq seq{rd(), rd(), rd(), rd(), rd(), rd()};
                return seq;
            }()
        );
    return rng;
}

template<typename Sseq>
void seed_rng(Sseq&& seq_)
{
    auto& rng = get_rng();
    rng.seed(std::forward<Sseq>(seq_));
}

/**
 * Make a random vector.
 **/
template <typename T, template<typename> typename Distr = std::normal_distribution>
T make_random_scalar
    (   std::optional<typename Distr<tulib::meta::real_type_t<T>>::param_type> params_ = std::nullopt
    )
{
    using real_type = tulib::meta::real_type_t<T>;
    auto& rng = get_rng();
    Distr<real_type> dist = params_ ? Distr<real_type>(params_.value()) : Distr<real_type>();
    if constexpr ( tulib::meta::is_complex_v<T> )
    {
        return T{dist(rng), dist(rng)};
    }
    else
    {
        return dist(rng);
    }
}

/**
 * Make a random vector.
 **/
template <typename T, template<typename> typename Distr = std::normal_distribution>
std::vector<T> make_random_vector
    (   size_t size_
    ,   std::optional<typename Distr<tulib::meta::real_type_t<T>>::param_type> params_ = std::nullopt
    )
{
    using real_type = tulib::meta::real_type_t<T>;
    std::vector<T> vec(size_);
    auto& rng = get_rng();
    Distr<real_type> dist = params_ ? Distr<real_type>(params_.value()) : Distr<real_type>();
    if constexpr ( tulib::meta::is_complex_v<T> )
    {
        std::generate(vec.begin(), vec.end(), [&] { return T{dist(rng), dist(rng)}; });
    }
    else
    {
        std::generate(vec.begin(), vec.end(), [&] { return dist(rng); });
    }
    return vec;
}

#if 0
#include <catch2/catch_approx.hpp>
/**
 * Check for comparison of complex
 *
 * @warning
 *  This requires the catch2/catch_approx.hpp header which is not installed.
 **/
template <typename T>
class complex_approx
    :   public Catch::Approx
{
public:
    static_assert(std::is_floating_point_v<T> || tulib::meta::is_complex_v<T>);

    explicit complex_approx(T x_) : _x(std::move(x_)) {}

    template
        <   typename U = T
        ,   std::enable_if_t<tulib::meta::is_complex_v<U>>* = nullptr
        >
    friend bool operator==(const U& y_, const complex_approx<U>& x_)
    {
        return  y_.real() == this->operator()(this->_x.real())
            &&  y_.imag() == this->operator()(this->_x.imag());
    }

private:
    T _x;
};
template <typename T> complex_approx(const T&) -> complex_approx<T>;
template <typename T> complex_approx(T&&) -> complex_approx<T>;
#endif

template <typename T>
bool check_complex_equal
    (   const T& x_
    ,   const T& y_
    ,   tulib::meta::real_type_t<T> epsilon_
    )
{
    if constexpr ( tulib::meta::is_complex_v<T> )
    {
        return  x_.real() == Approx(y_.real()).epsilon(epsilon_)
            &&  x_.imag() == Approx(y_.imag()).epsilon(epsilon_);
    }
    else
    {
        return x_ == Approx(y_).epsilon(epsilon_);
    }
}


}
