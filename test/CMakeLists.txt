set(TESTNAME tulib_test)
add_executable(${TESTNAME} math_test.cpp meta_test.cpp util_test.cpp)
target_compile_features(${TESTNAME} PRIVATE cxx_std_17)
target_include_directories(${TESTNAME} PRIVATE include)
find_package(Catch2 REQUIRED)
target_compile_options(${TESTNAME} PRIVATE -Wall -Wextra -Werror)
target_include_directories(
    ${TESTNAME}
    PRIVATE
    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
    )
target_link_libraries(${TESTNAME} PRIVATE Catch2::Catch2WithMain)
