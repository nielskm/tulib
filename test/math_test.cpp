/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file math_test.cpp
 * @author Niels Kristian Kjærgård Madsen
 * @date Fri Jun 24 2022
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *********************************************************************************************/

#include <catch2/catch.hpp>

#include <tulib/math.hpp>
#include <test/math_test.hpp>

TEST_CASE("Container squared norm", "[math]")
{
    // Norm2 test
    tulib::test::container_norm2_test::run<float>();
    tulib::test::container_norm2_test::run<double>();
    tulib::test::container_norm2_test::run<std::complex<float>>();
    tulib::test::container_norm2_test::run<std::complex<double>>();
}

TEST_CASE("Container scaling", "[math]")
{
    // Norm2 test
    tulib::test::container_scale_test::run<float>();
    tulib::test::container_scale_test::run<double>();
    tulib::test::container_scale_test::run<std::complex<float>>();
    tulib::test::container_scale_test::run<std::complex<double>>();
}

TEST_CASE("Container size", "[math]")
{
    // Size test
    tulib::test::container_size_test::run<float>();
    tulib::test::container_size_test::run<double>();
    tulib::test::container_size_test::run<std::complex<float>>();
    tulib::test::container_size_test::run<std::complex<double>>();
}

TEST_CASE("Container dot product", "[math]")
{
    // Dot-product test
    tulib::test::container_dot_product_test::run<float>();
    tulib::test::container_dot_product_test::run<double>();
    tulib::test::container_dot_product_test::run<std::complex<float>>();
    tulib::test::container_dot_product_test::run<std::complex<double>>();
}

TEST_CASE("Container axpy", "[math]")
{
    // Dot-product test
    tulib::test::container_axpy_test::run<float>();
    tulib::test::container_axpy_test::run<double>();
    tulib::test::container_axpy_test::run<std::complex<float>>();
    tulib::test::container_axpy_test::run<std::complex<double>>();
}

TEST_CASE("Container linear combination", "[math]")
{
    // Dot-product test
    tulib::test::container_linear_combination_test::run<float>();
    tulib::test::container_linear_combination_test::run<double>();
    tulib::test::container_linear_combination_test::run<std::complex<float>>();
    tulib::test::container_linear_combination_test::run<std::complex<double>>();
}

TEST_CASE("Container scaled error", "[math]")
{
    // Dot-product test
    tulib::test::container_scaled_error_test::run<float>();
    tulib::test::container_scaled_error_test::run<double>();
    tulib::test::container_scaled_error_test::run<std::complex<float>>();
    tulib::test::container_scaled_error_test::run<std::complex<double>>();
}
