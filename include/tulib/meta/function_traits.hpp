/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file function_traits.hpp
 * @author Niels Kristian Kjærgård Madsen
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/
#pragma once

#include <type_traits>
#include <functional>

namespace tulib::meta
{
namespace detail
{

/**
 * Declare function traits
 **/
template<typename F> struct function_traits;

/**
 * Implement for functions
 **/
template
   <  typename R
   ,  typename... Args
   >
struct function_traits<R(Args...)>
{
   using return_type = R;
   using argument_types = std::tuple<Args...>;
	using function_type = std::function<R(Args...)>;
   static constexpr std::size_t argc = sizeof...(Args);

   template
      <  std::size_t N
      >
   struct argument
   {
      static_assert(N < argc, "Argument number out of range!");

      using type = typename std::tuple_element<N, std::tuple<Args...>>::type;
   };
};

/**
 * Specialize for function pointers
 **/
template<typename R, typename... Args> struct function_traits<R(*)(Args...)> : public function_traits<R(Args...)> {};

/**
 * Specialize for member functions
 **/
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...)> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) const> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) volatile> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) const volatile> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) &> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) const &> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) volatile &> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) const volatile &> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) &&> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) const &&> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) volatile &&> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) const volatile &&> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) noexcept> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) const noexcept> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) volatile noexcept> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) const volatile noexcept> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) & noexcept> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) const & noexcept> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) volatile & noexcept> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) const volatile & noexcept> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) && noexcept> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) const && noexcept> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) volatile && noexcept> : public function_traits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct function_traits<R(C::*)(Args...) const volatile && noexcept> : public function_traits<R(Args...)> {};

/**
 * Default implementation for functors and lambda functions
 * Use function_traits on operator()
 **/
template<typename F> struct function_traits : public function_traits<decltype(&F::operator())> {};
template<typename F> struct function_traits<F&> : public function_traits<F> {};
template<typename F> struct function_traits<F&&> : public function_traits<F> {};

} /* namespace detail */

/**
 * Get return type of function ptr, lambda, functor, member function, etc.
 **/
template<typename T> using return_type = typename detail::function_traits<T>::return_type;

/**
 * Get number of arguments to function ptr, lambda, functor, member function, etc.
 **/
template<typename T> inline constexpr std::size_t argument_count = detail::function_traits<T>::argc;

/**
 * Get argument type of function ptr, lambda, functor, member function, etc.
 **/
template<std::size_t N, typename T> using argument_type = typename detail::function_traits<T>::template argument<N>::type;

/**
 * Get tuple of argument types.
 **/
template<typename T> using arguments_tuple_t = typename detail::function_traits<T>::argument_types;

/**
 *
 **/
template<typename T> using function_t = typename detail::function_traits<T>::function_type;

} /* namespace tulib::meta */
