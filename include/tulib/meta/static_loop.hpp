/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file static_loop.hpp
 * @author Niels Kristian Kjærgård Madsen
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/

#pragma once

namespace tulib::meta
{
namespace detail
{
template <std::size_t I>
struct iteration_index
{
   constexpr operator std::size_t() const
   {
      return I;
   }
};
} /* namespace detail */

//! Static for loop
template
   <  std::size_t I
   ,  std::size_t N
   ,  std::size_t Incr = 1
   >
struct static_for
{
   // Assert that I is smaller than N
   static_assert(I < N);

   // Assert that the loop hits N
   static_assert( (N-I) % Incr == 0 );

   //! Function that is called recursively
   template <typename F>
   static void loop
      (  F&& f
      )
   {
      f(detail::iteration_index<I>());
      if constexpr ( I + Incr != N )
      {
         static_for<I+Incr,N,Incr>::loop(std::forward<F>(f));
      }
   }
};

} /* namespace tulib::meta */
