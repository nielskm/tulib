/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file is_iterable.hpp
 * @author Niels Kristian Kjærgård Madsen
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/
#pragma once

#include <tulib/meta/is_detected.hpp>

namespace tulib::meta
{
namespace detail
{

//! Allow ADL with custom begin and end
using std::begin;
using std::end;

template <typename T>
auto is_iterable(int)
   -> decltype
         (  (begin(std::declval<T&>()) != end(std::declval<T&>()))   // Check for begin/end and operator !=
         ,  void()                                                   // Handle side effects of operator ,
         ,  ++std::declval<decltype(begin(std::declval<T&>()))&>()   // operator++
         ,  void(*begin(std::declval<T&>()))                         // operator*
         ,  std::true_type{}
         );

template <typename T>
std::false_type is_iterable(...);

} /* namespace detail */

//! Check if a containter can be iterated over
template <typename T>
constexpr bool is_iterable_v = decltype(detail::is_iterable<T>(0))::value;

} /* namespace tulib::meta */
