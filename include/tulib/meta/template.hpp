/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file template.hpp
 * @author Niels Kristian Kjærgård Madsen
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/

#pragma once

#include <tulib/meta/complex.hpp>

namespace tulib::meta
{

//!@{
template <typename T>
struct is_template : public std::false_type {};

template
   <  typename... Args
   ,  template<typename...> typename Template
   >
struct is_template<Template<Args...>> : public std::true_type {};

template <typename T>
constexpr bool is_template_v = is_template<T>::value;
//!@}

//!@{
template <typename T>
struct template_argument
{
   using type = T;
};
template
   <  typename T
   ,  template<typename...> typename Template
   >
struct template_argument<Template<T>>
{
   using type = T;
};

template <typename T>
using template_argument_t = typename template_argument<T>::type;
//!@}

//!@{
template <typename T>
struct recursive_template_argument
{
   using type = T;
};
template
   <  typename T
   ,  template<typename...> typename Template
   >
struct recursive_template_argument<Template<T>>
{
   using type = typename recursive_template_argument<T>::type;
};

template <typename T>
using recursive_template_argument_t = typename recursive_template_argument<T>::type;
//!@}

//!@{
template <typename T>
struct template_argument_or_complex
{
   using type = T;
};
template
   <  typename T
   ,  template<typename...> typename Template
   >
struct template_argument_or_complex<Template<T>>
{
   using type = std::conditional_t<tulib::meta::is_complex_v<Template<T>>, Template<T>, T>;
};
template <typename T>
using template_argument_or_complex_t = typename template_argument_or_complex<T>::type;
//!@}

//!@{
template <typename T>
struct recursive_template_argument_or_complex
{
   using type = T;
};
template
   <  typename T
   ,  template<typename...> typename Template
   >
struct recursive_template_argument_or_complex<Template<T>>
{
   using type = std::conditional_t<tulib::meta::is_complex_v<Template<T>>, Template<T>, typename recursive_template_argument_or_complex<T>::type>;
};
template <typename T>
using recursive_template_argument_or_complex_t = typename recursive_template_argument_or_complex<T>::type;
//!@}

} /* namespace tulib::meta */
