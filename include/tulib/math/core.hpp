/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file core.hpp
 * @author Niels Kristian Kjærgård Madsen
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/

#pragma once

#include <tulib/meta/complex.hpp>

namespace tulib::math
{
/**
 * Complex conjugate returning 'correct' type
 *
 * @param v
 * @return
 *    conj(v)
 **/
//!@{
template
   <  typename T
   ,  std::enable_if_t<tulib::meta::is_complex_v<T> >* = nullptr
   >
inline constexpr T conj
   (  const T& v
   )
{
   return std::conj(v);
}
template
   <  typename T
   ,  std::enable_if_t<!tulib::meta::is_complex_v<T> >* = nullptr
   >
inline constexpr T conj
   (  const T& v
   )
{
   return v;
}
//!@}

/**
 * Squared modulus
 *
 * @param v
 * @return
 *    |v|^2
 **/
//!@{
template
   <  typename T
   ,  std::enable_if_t<tulib::meta::is_complex_v<T> >* = nullptr
   >
inline constexpr tulib::meta::real_type_t<T> abs2
   (  const T& v
   )
{
   // Note: std::norm is slightly faster that the naive real*real + imag*imag.
   return std::norm(v);
}
template
   <  typename T
   ,  std::enable_if_t<!tulib::meta::is_complex_v<T> >* = nullptr
   >
inline constexpr tulib::meta::real_type_t<T> abs2
   (  const T& v
   )
{
   return v*v;
}
//!@}

/**
 * Compute a Givens rotation
 *
 * See Algorithm 1 of:
 * David Bindel, James Demmel, William Kahan, and Osni Marques. 2002. On computing givens rotations reliably and efficiently. ACM Trans. Math. Softw. 28, 2 (June 2002), 206–238.
 *
 * @param f_
 * @param g_
 * @return
 *    array(c, s, r)
 **/
template<typename T>
std::array<T, 3> givens_rotation
   (  const T& f_
   ,  const T& g_
   )
{
   auto sign = [](const T& x_)
   {
      if ( x_ == T(0) )
      {
         return T(1.);
      }
      else
      {
         return x_ / std::abs(x_);
      }
   };

   if ( g_ == T(0) )
   {
      return {T(1), T(0), f_};
   }
   else if ( f_ == T(0) )
   {
      return {T(0), sign(conj(g_)), std::abs(g_)};
   }
   else
   {
      // Is there a hypot alternative for std::complex?
      const auto norm = meta::is_complex_v<T> ? std::sqrt(abs2(f_) + abs2(g_)) : std::hypot(f_, g_);
      const auto sf = sign(f_);
      return
      {  std::abs(f_) / norm
      ,  sf*conj(g_) / norm
      ,  sf*norm
      };
   }
}


} /* namespace tulib::math */
