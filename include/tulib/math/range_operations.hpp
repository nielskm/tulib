/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file range_operations.hpp
 * @author Niels Kristian Kjærgård Madsen
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/

#pragma once
#include <tulib/util/exceptions.hpp>

namespace tulib::math::detail
{

/**
 *
 **/
template
   <  typename RET_T
   ,  typename VEC_T
   >
[[deprecated("Use parallel STL instead or SIMD instructions in C++20")]]
RET_T dot_product_impl
   (  const VEC_T& aLeft
   ,  const VEC_T& aRight
   )
{
   // Allow for custom begin, end, advance, and distance by using ADL
   using std::begin;
   using std::end;
   using std::advance;
   using std::distance;

   using ret_t = RET_T;
   ret_t result1 = 0;
   ret_t result2 = 0;
   auto lit1 = begin(aLeft);
   auto lit2 = begin(aLeft); advance(lit2, 1);
   auto rit1 = begin(aRight);
   auto rit2 = begin(aRight); advance(rit2, 1);
   auto lend = end(aLeft);
   auto dist = distance(lit1, lend);
   // For even number of data, lit1 will hit lend
   if (  dist % 2 == 0
      )
   {
      for(; lit1 != lend; advance(lit1, 2), advance(lit2, 2), advance(rit1, 2), advance(rit2, 2))
      {
         result1 += tulib::math::conj(*lit1) * *rit1;
         result2 += tulib::math::conj(*lit2) * *rit2;
      }
   }
   // For odd number, lit2 will hit end, but the last element of beg1 will need to be added after the loop
   else
   {
      for(; lit2 != lend; advance(lit1, 2), advance(lit2, 2), advance(rit1, 2), advance(rit2, 2))
      {
         result1 += tulib::math::conj(*lit1) * *rit1;
         result2 += tulib::math::conj(*lit2) * *rit2;
      }
      result1 += tulib::math::conj(*lit1) * *rit1;
   }
   return result1 + result2;
}

/**
 *
 **/
template
   <  typename RET_T
   ,  typename VEC_T
   >
[[deprecated("Use parallel STL instead or SIMD instructions in C++20")]]
RET_T norm2_impl
   (  const VEC_T& aC
   )
{
   // Allow for custom begin, end, advance, and distance by using ADL
   using std::begin;
   using std::end;
   using std::advance;
   using std::distance;

   using ret_t = RET_T;
   ret_t result1 = 0;
   ret_t result2 = 0;
   auto beg1 = begin(aC);
   auto beg2 = begin(aC); advance(beg2, 1);
   auto e = end(aC);
   auto dist = distance(beg1, e);
   // For even number of data, beg1 will hit end
   if (  dist % 2 == 0
      )
   {
      for(; beg1 != e; advance(beg1, 2), advance(beg2, 2))
      {
         result1 += tulib::math::abs2(*beg1);
         result2 += tulib::math::abs2(*beg2);
      }
   }
   // For odd number, beg2 will hit end, but the last element of beg1 will need to be added after the loop
   else
   {
      for(; beg2 != e; advance(beg1, 2), advance(beg2, 2))
      {
         result1 += tulib::math::abs2(*beg1);
         result2 += tulib::math::abs2(*beg2);
      }
      result1 += tulib::math::abs2(*beg1);
   }
   return result1 + result2;
}

//! Tiled scaled error2
template
   <  typename RET_T
   ,  typename VEC_T
   ,  typename ABS_T
   >
RET_T scaled_error2_impl
   (  const VEC_T& aDeltaY
   ,  const VEC_T& aYNew
   ,  const VEC_T& aYOld
   ,  ABS_T aAbs
   ,  ABS_T aRel
   ,  bool aMax
   )
{
   // Allow for custom begin, end, advance, and distance by using ADL
   using std::begin;
   using std::end;
   using std::advance;
   using std::distance;

   using ret_t = RET_T;
   ret_t result1 = 0;
   ret_t sc1 = 0;

   auto it_dy1 = begin(aDeltaY);
   auto it_yn1 = begin(aYNew);
   auto it_yo1 = begin(aYOld);
   auto end_dy = end(aDeltaY);

   auto size_dy = distance(it_dy1, end_dy);

   if (  aMax
      )
   {
      for(; it_dy1 != end_dy; advance(it_dy1, 1), advance(it_yo1, 1), advance(it_yn1, 1))
      {
         sc1 = aAbs + aRel*std::max(std::abs(*it_yn1), std::abs(*it_yo1));
         result1 = std::max(result1, tulib::math::abs2(*it_dy1 / sc1));
      }

      return result1;
   }
   else
   {
      ret_t result2 = 0;
      auto it_dy2 = begin(aDeltaY); advance(it_dy2, 1);
      auto it_yn2 = begin(aYNew); advance(it_yn2, 1);
      auto it_yo2 = begin(aYOld); advance(it_yo2, 1);
      ret_t sc2 = 0;

      // For even number of data, it_dy1 will hit end_dy
      if (  size_dy % 2 == 0
         )
      {
         for(; it_dy1 != end_dy; advance(it_dy1, 2), advance(it_dy2, 2), advance(it_yn1, 2), advance(it_yn2, 2), advance(it_yo1, 2), advance(it_yo2, 2))
         {
            sc1 = aAbs + aRel*std::max(std::abs(*it_yn1), std::abs(*it_yo1));
            result1 += tulib::math::abs2(*it_dy1 / sc1);
            sc2 = aAbs + aRel*std::max(std::abs(*it_yn2), std::abs(*it_yo2));
            result2 += tulib::math::abs2(*it_dy2 / sc2);
         }
      }
      // For odd number, it_dy2 will end_dy, but the last element of it_dy1 will need to be added after the loop
      else
      {
         for(; it_dy2 != end_dy; advance(it_dy1, 2), advance(it_dy2, 2), advance(it_yn1, 2), advance(it_yn2, 2), advance(it_yo1, 2), advance(it_yo2, 2))
         {
            sc1 = aAbs + aRel*std::max(std::abs(*it_yn1), std::abs(*it_yo1));
            result1 += tulib::math::abs2(*it_dy1 / sc1);
            sc2 = aAbs + aRel*std::max(std::abs(*it_yn2), std::abs(*it_yo2));
            result2 += tulib::math::abs2(*it_dy2 / sc2);
         }
         sc1 = aAbs + aRel*std::max(std::abs(*it_yn1), std::abs(*it_yo1));
         result1 += tulib::math::abs2(*it_dy1 / sc1);
      }

      return result1 + result2;
   }
}


} /* namespace tulib::math::detail */
