/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file io.hpp
 * @author Niels Kristian Kjærgård Madsen
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/
#pragma once

#include <map>
#include <set>
#include <ostream>
#include <functional>
#include <mutex>

namespace tulib::io
{
//! Forward decl
class writer_collection;
template<typename T> writer_collection& operator<<(writer_collection&, const T&);
writer_collection& operator<<(writer_collection&, std::ostream& (*)(std::ostream&));

//! Mutex
inline std::mutex& get_writer_collection_mutex()
{
   static std::mutex m;
   return m;
}

//! Set iolevel
struct setiolevel
{
   int iol = 5;
   setiolevel(int i_) : iol(i_) {}
};
//! Increment or decrement indentation level
struct incrindent {};
struct decrindent {};

/**
 * Collection of ostreams possibly using different output-detail levels.
 * Output is also indented.
 **/
class writer_collection
{
public:
   //! Constructor from set of ostreams with max. iolevel
   writer_collection
      (  const std::vector<std::pair<std::reference_wrapper<std::ostream>, int>>& os_
      )
      :  _ostreams(os_)
   {
   }

   //! Constructor from single ostream
   writer_collection
      (  std::ostream& os_
      ,  int maxiolevel_
      )
      :  _ostreams({{std::ref(os_), maxiolevel_}})
   {
   }

   //! Friend
   template<typename T> friend writer_collection& operator<<(writer_collection&, const T&);
   friend writer_collection& operator<<(writer_collection&, std::ostream& (*)(std::ostream&));

   //! Get current iolevel
   int iolevel() const noexcept { return this->_iolevel; }

   //! Get current indentation level
   int idtlevel() const noexcept { return this->_idtlevel; }

   //! Get tabsize
   int tabsize() const noexcept { return this->_tabsize; }

private:
   //! Set of ostreams with maximum iolevel
   std::vector<std::pair<std::reference_wrapper<std::ostream>, int>> _ostreams;

   //! Indentation size
   int _tabsize = 3;

   //! Current output level
   int _iolevel = 0;

   //! Current indentation level
   int _idtlevel = 0;

   //! Print
   template<typename T>
   void print(const T& t_)
   {
      for(auto& os : _ostreams)
      {
         if ( this->_iolevel <= os.second )
         {
            std::lock_guard<std::mutex> lock(get_writer_collection_mutex());
            os.first.get() << std::string(this->_tabsize*this->_idtlevel, ' ') << t_;
         }
      }
   }
};

//! Output operator
template<typename T>
writer_collection& operator<<(writer_collection& os_, const T& t_)
{
   os_.print(t_);
   return os_;
}

//! Specialization for setiolevel
template<>
inline writer_collection& operator<< <setiolevel>(writer_collection& os_, const setiolevel& io_)
{
   os_._iolevel = io_.iol;
   return os_;
}

//! Specialization for incrindent
template<>
inline writer_collection& operator<< <incrindent>(writer_collection& os_, const incrindent&)
{
   ++os_._idtlevel;
   return os_;
}

//! Specialization for decrindent
template<>
inline writer_collection& operator<< <decrindent>(writer_collection& os_, const decrindent&)
{
   if ( os_._idtlevel > 0 ) --os_._idtlevel;
   return os_;
}

//! Specializations for functions such as std::endl
inline writer_collection& operator<<(writer_collection& os_, std::ostream& (*func_)(std::ostream&))
{
   for(auto& os : os_._ostreams)
   {
      if ( os_.iolevel() <= os.second )
      {
         func_(os.first);
      }
   }
   return os_;
}

namespace detail
{

struct writer_manipulation
{
   using type = size_t;
   static constexpr type iolevel = 0;
   static constexpr type indentation = 1;
};

/**
 *
 **/
template<size_t Type>
class scoped_manipulator
{
public:
   //! Constructor modifies writer_collection
   scoped_manipulator
      (  writer_collection& writers_
      ,  int iol_ = 0
      )
      :  _writers(writers_)
      ,  _prev_iolevel(writers_.iolevel())
   {
      if constexpr ( Type == writer_manipulation::iolevel )
      {
         writers_ << (setiolevel(iol_));
      }
      else if constexpr ( Type == writer_manipulation::indentation )
      {
         writers_ << incrindent();
      }
   }

   //! Destructor reverts
   ~scoped_manipulator()
   {
      if constexpr ( Type == writer_manipulation::iolevel )
      {
         _writers << (setiolevel(_prev_iolevel));
      }
      else if constexpr ( Type == writer_manipulation::indentation )
      {
         _writers << decrindent();
      }
   }

private:
   //! Holds reference to writer_collection
   writer_collection& _writers;

   //! Value of previous iolevel
   int _prev_iolevel = 0;
};

} /* namespace detail */

// Macros for setting iolevel and indentation of a scope (NB: perhaps add __COUNTER__ to names?)
#define SCOPE_IOLEVEL(wcol, iol) detail::scoped_manipulator<::tulib::io::detail::writer_manipulation::iolevel> __TULIB_SCOPE_IOLEVEL(wcol, iol);
#define SCOPE_INDENT(wcol) detail::scoped_manipulator<::tulib::io::detail::writer_manipulation::indentation> __TULIB_SCOPE_INDENT(wcol);

} /* namespace tulib::io */
