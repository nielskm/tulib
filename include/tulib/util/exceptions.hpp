/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file exceptions.hpp
 * @author Niels Kristian Kjærgård Madsen
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/

#pragma once

#include <exception>
#include <string>

namespace tulib::except
{

/**
 * Undefined exception
 **/
class error
   :  public std::exception
{
   private:
      //! Message
      std::string _message = "";

   public:
      //! Constructor
      error
         (  const std::string& a_mess
         ,  const char* a_filename
         ,  unsigned a_linenumber
         )
         :  _message
               (  a_mess + " File: " + std::string(a_filename) + ", Line: " + std::to_string(a_linenumber)
               )
      {
      }

      //! What
      const char* what
         (
         )  const noexcept override
      {
         return this->_message.c_str();
      }
};

} /* namespace tulib::except */

// TODO: Update to std::source_location when switching to C++20
#define TULIB_EXCEPTION(WHAT)  \
   throw ::tulib::except::error(WHAT, __FILE__, __LINE__);
