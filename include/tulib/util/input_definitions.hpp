/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file input_definitions.hpp
 * @author Niels Kristian Kjærgård Madsen
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/

#pragma once

#include <any>
#include <map>
#include <tulib/util/exceptions.hpp>

namespace tulib::util
{

/**
 *
 **/
template
   <  typename KeyType
   ,  template<typename...> typename MapTmplType = std::unordered_map
   ,  typename IntegerType = int
   ,  typename FloatType = double
   >
class input_definitions
   :  private MapTmplType<KeyType, std::any>
{
   public:
      //! Aliases
      using integer_type = IntegerType;
      using float_type = FloatType;
      template<typename T> using map_tmpl_type = MapTmplType<KeyType, T>;
      using map_type = map_tmpl_type<std::any>;
      template<typename T> using defaults_type = MapTmplType<T, map_type>;
      template<typename T> using defaults_map_type = map_tmpl_type<defaults_type<T>>;

      /**
       * Members from std::map which are visible.
       **/
      using map_type::size;
      using map_type::clear;
      using map_type::empty;
      using map_type::begin;
      using map_type::cbegin;
      using map_type::end;
      using map_type::cend;

      /**
       * Validate and set defaults
       * Defaults may depend on settings, i.e. if one key is set to A, a different set of defaults may be used that if it was B.
       * Thus, a defaults map is given per default-controlling key.
       **/
      template
         <  typename T
         >
      bool validate
         (  const defaults_map_type<T>& defaults_
         )
      {
         try
         {
            for(const auto& d : defaults_)
            {
               const auto& key = d.first;
               const auto& defaults_map = d.second;

               // Require that the key is set
               auto it = this->find(key);
               if ( it == this->end() )
               {
                  return false;
               }

               // Pull out the correct defaults
               const auto& defaults_k = defaults_map.at(std::any_cast<T>(it->second));

               // Set all keywords that are not already present in this
               for(const auto& d2 : defaults_k)
               {
                  const auto& key2 = d2.first;
                  const auto& default_value = d2.second;

                  // If not present, set the default value.
                  if ( this->find(key2) == this->end() )
                  {
                     this->add(key2, default_value);
                  }
               }
            }

            // Valid if we got here
            return true;
         }
         catch(const std::out_of_range&)
         {
            return false;
         }
         catch(...)
         {
            TULIB_EXCEPTION("Caught an unexpected exception.");
         }
      }

      /**
       * Insert functionality.
       * All integer types are converted to integer_type and all floating-point types to float_type
       * before wrapping in std::any.
       * Thus, std::any_cast will be safe when casting to input_definitions::input_type, etc. and
       * exceptions are avoided when trying to get an int from long, etc.
       **/
      template<typename T>
      auto add
         (  const KeyType& key_
         ,  T&& value_
         )
      {
         using value_type = std::decay_t<T>;
         if constexpr ( std::is_integral_v<value_type> && !std::is_same_v<value_type, bool> )
         {
            return this->insert({key_, std::make_any<integer_type>(value_)});
         }
         else if constexpr ( std::is_floating_point_v<value_type> )
         {
            return this->insert({key_, std::make_any<float_type>(value_)});
         }
         else if constexpr ( std::is_same_v<value_type, std::any> )
         {
            if ( this->find(key_) == this->end() )
            {
               this->insert({key_, value_});
            }
            else
            {
               this->operator[](key_) = value_;
            }
         }
         else
         {
            return this->insert({key_, std::make_any<T>(value_)});
         }
      }

      /**
       * Get functionality.
       **/
      template<typename T>
      auto get
         (  const KeyType& key_
         )  const
      {
         using value_type = std::decay_t<T>;
         try
         {
            if constexpr ( std::is_integral_v<value_type> && !std::is_same_v<value_type, bool> )
            {
               return T(std::any_cast<integer_type>(this->at(key_)));
            }
            else if constexpr ( std::is_floating_point_v<value_type> )
            {
               return T(std::any_cast<float_type>(this->at(key_)));
            }
            else
            {
               return std::any_cast<T>(this->at(key_));
            }
         }
         catch(const std::out_of_range&)
         {
            TULIB_EXCEPTION("Tried to get nonexisting key from input_definitions.");
         }
         catch(const std::bad_any_cast&)
         {
            TULIB_EXCEPTION("Bad any_cast in input_definitions. Tried to get type '" + std::string(typeid(T).name()) + "' from any holding '" + std::string(this->at(key_).type().name()) + "'.");
         }
      }
};

} /* namespace tulib::util */
