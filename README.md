# TUlib : the Template Utility library

A collection of tools for template metaprogramming and convenient wrappers for performing mathematical operations on different container structures.

## Installation

1) Clone the repository

```bash
git clone git@gitlab.com:tulib/tulib.git
```

2) Build and install:

As TUlib is header-only, you only need access to the source files. But you may use CMake for building the unit tests and configuring the targets for easy CMake integration.

```bash
mkdir <build-dir>
cd <build-dir>
cmake <source-dir> -DCMAKE_BUILD_TYPE=<build-type> -DBUILD_TESTS=<ON/OFF>
cmake --build .
cmake --install . --prefix=<install-prefix>
```

## Features

### General container math wrappers

TUlib implements general wrappers for computing e.g. norms and dot products of general nested containers such as `std::vector<std::vector<double>>`, `std::tuple<double, std::vector<double>, std::pair<double, double>>`, etc. The functions will work on any container that implements a function with an appropriate name (e.g. `norm`, `dot_product`, `scale`, etc.) or supports standard C++ iterator interface.

The functions currently implemented are:

- `wrap_size`: Returns the number of elements in the container (i.e. the total number of numbers in a nested container).
- `wrap_scale`: Multiplies all elements by a scalar value.
- `wrap_axpy`: Adds a scaled container in-place (y = a*x + y).
- `wrap_dot_product`: Computes the dot product between two containers.
- `wrap_norm2`: Returns the squared (Frobenius) norm of the container.
- `wrap_norm`: The square root of `wrap_norm2`.
- `wrap_linear_combination`: Returns a linear combination of a container of containers using a container of coefficients.

The implementation decides at compile time how to perform the computation:

- All functions start by checking if a proper function is implemented elsewhere that takes the provided types as arguments. Currently both snake_case and CamelCase function names are checked (e.g. `Norm2(ContainerType x)` _and_ `norm2(ContainerType x)`).
- If no appropriate function is found, the compiler checks if the container itself implements the method (e.g. `ContainerType::Norm2()` and `ContainerType::norm2()`).
- If no user-defined functions are found, the default implementation is invoked. This is implemented using a standard iterator interface, i.e. it works if your container is iterable (`for(auto x : your_container)` works).

Experimental: The default (iterator-based) implementation of the algorithms can be run in parallel using `std::execution` by adding `#define TULIB_PAR_STL` prior to including the `tulib/math/container_math_wrappers.hpp` header.

### Template meta-programming utilities

A number of handy tools for template metaprogramming are included in the `tulib/meta` directory. This includes:

- `is_detected.hpp`: A compile-time check for determining e.g. if a class implements a given method.
- `function_traits.hpp`: Extract return types and argument types of a given function pointer, functor, class member function, or lambda function (i.e. any callable object).
- `static_loop.hpp`: A compile-time unrolled loop of fixed length. Can be used for iterating through `std::tuple`.
- `is_iterable.hpp`: Checks if a given type is iterable.
- `complex.hpp`: Provides `is_complex_v` and `real_type_t` for determining whether a type is an instance of `std::complex` and for extracting the underlying floating-point type.
- `template.hpp`: Provides `is_template_v` for checking if a type is a template as well as a couple of other utilities.
